package selects

import (
	"fmt"
	"net/http"
	"time"
)

func Racer(w1, w2 string, timeout time.Duration) (winner string, error error) {
	// w1Duration := measureTheTime(w1)
	// w2Duration := measureTheTime(w2)

	// if w1Duration < w2Duration {
	// 	return w1
	// }
	// return w2

	select {
	case <-ping(w1):
		return w1, nil
	case <-ping(w2):
		return w2, nil
	case <-time.After(timeout):
		return "", fmt.Errorf("timed out waiting for %s and %s", w1, w2)
	}
}

func ping(url string) chan struct{} {
	ch := make(chan struct{})
	go func() {
		http.Get(url)
		close(ch)
	}()
	return ch
	// start := time.Now()
	// http.Get(url)
	// return time.Since(start)
}
