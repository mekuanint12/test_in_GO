package selects

import (
	"net/http"
	"net/http/httptest"
	"testing"
	"time"
)

func makeTheServerLate(delay time.Duration) *httptest.Server {
	return httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		time.Sleep(delay)
		w.WriteHeader(http.StatusOK)
	}))
}

const tenSecondTimeout = 10 * time.Millisecond

func TestRacer(t *testing.T) {

	t.Run("compares speeds of servers, returning the url of the fastest one", func(t *testing.T) {

		slowServer := makeTheServerLate(20 * time.Millisecond)
		fastServer := makeTheServerLate(0 * time.Millisecond)

		defer slowServer.Close()
		defer fastServer.Close()

		slowURL := slowServer.URL
		fastURL := fastServer.URL

		want := fastURL
		got, err := Racer(slowURL, fastURL, tenSecondTimeout)

		if err != nil {
			t.Fatalf("did not expect an error but got one %v", err)
		}

		if got != want {
			t.Errorf("got %q, want %q", got, want)
		}
	})

	t.Run("returns an error if a server doesn't respond within the specified time", func(t *testing.T) {
		server := makeTheServerLate(25 * time.Millisecond)

		defer server.Close()

		_, err := Racer(server.URL, server.URL, 20*time.Millisecond)

		if err == nil {
			t.Error("expected an error but didn't get one")
		}
	})
}

// func TestRacer(t *testing.T) {
// 	// slowURL := "https://www.facebook.com"
// 	// fastURL := "https://www.quii.dev"

// 	// slowServer := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
// 	// 	time.Sleep(20 * time.Millisecond)
// 	// 	w.WriteHeader(http.StatusOK)
// 	// }))
// 	// fastServer := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
// 	// 	w.WriteHeader(http.StatusOK)
// 	// }))

// slowServer := makeTheServerLate(20 * time.Millisecond)
// fastServer := makeTheServerLate(0 * time.Millisecond)

// 	defer slowServer.Close()
// 	defer fastServer.Close()

// 	slowURL := slowServer.URL
// 	fastURL := fastServer.URL
// 	want := fastURL
// 	got, err := Racer(slowURL, fastURL, tenSecondTimeout)
// 	// _, err := Racer()

// 	if err == nil {
// 		t.Error("expected an error but didn't get one")
// 	}

// 	if got != want {
// 		t.Errorf("got %q , want %q ", got, want)
// 	}
// }
