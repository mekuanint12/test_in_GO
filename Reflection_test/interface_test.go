package refelect

import (
	"reflect"
	"testing"
)

type Person struct {
	Name    string
	Profile Profile
}

type Profile struct {
	Age  int
	City string
}

func assertContains(t testing.TB, haystack []string, needle string) {
	t.Helper()
	contains := false
	for _, x := range haystack {
		if x == needle {
			contains = true
		}
	}
	if !contains {
		t.Errorf("expected %+v to contain %q but it didn't", haystack, needle)
	}
}

func TestWalk(t *testing.T) {

	cases := []struct {
		Name          string
		Input         interface{}
		ExpectedCalls []string
	}{
		{
			"struct with one string field",
			struct {
				Name string
			}{"Meku"},
			[]string{"Meku"},
		},
		{
			"struct with two string fields",
			struct {
				Name string
				City string
			}{"Meku", "Addis Abeba"},
			[]string{"Meku", "Addis Abeba"},
		},
		{
			"struct with non string field",
			struct {
				Name string
				Age  int
			}{"Meku", 25},
			[]string{"Meku"},
		},
		{
			"nested fields",
			struct {
				Name    string
				Profile struct {
					Age  int
					City string
				}
			}{"Meku", struct {
				Age  int
				City string
			}{25, "Addis Abeba"}},
			[]string{"Meku", "Addis Abeba"},
		},
		{
			"nested fields",
			Person{
				"Meku",
				Profile{25, "Addis Abeba"},
			},
			[]string{"Meku", "Addis Abeba"},
		},
		{
			"pointers to things",
			&Person{
				"Chris",
				Profile{33, "London"},
			},
			[]string{"Chris", "London"},
		},
		{
			"slices",
			[]Profile{
				{25, "Addis Abeba"},
				{26, "Maki"},
			},
			[]string{"Addis Abeba", "Maki"},
		},
		{
			"arrays",
			[2]Profile{
				{25, "Addis Abeba"},
				{26, "Maki"},
			},
			[]string{"Addis Abeba", "Maki"},
		},
		{
			"maps",
			map[string]string{
				"Foo": "Bar",
				"Baz": "Boz",
			},
			[]string{"Bar", "Boz"},
		},
	}

	for _, test := range cases {
		t.Run(test.Name, func(t *testing.T) {
			var got []string
			walk(test.Input, func(input string) {
				got = append(got, input)
			})

			if !reflect.DeepEqual(got, test.ExpectedCalls) {
				t.Errorf("got %v, want %v", got, test.ExpectedCalls)
			}
		})
	}

	t.Run("with maps", func(t *testing.T) {
		aMap := map[string]string{
			"Foo": "Bar",
			"Baz": "Boz",
		}

		var got []string
		walk(aMap, func(input string) {
			got = append(got, input)
		})

		assertContains(t, got, "Bar")
		assertContains(t, got, "Boz")
	})
	t.Run("with channels", func(t *testing.T) {
		aChannel := make(chan Profile)

		go func() {
			aChannel <- Profile{33, "Meku"}
			aChannel <- Profile{25, "Legese"}
			close(aChannel)
		}()

		var got []string
		want := []string{"Meku", "Legese"}

		walk(aChannel, func(input string) {
			got = append(got, input)
		})

		if !reflect.DeepEqual(got, want) {
			t.Errorf("got %v, want %v", got, want)
		}
	})
	t.Run("with function", func(t *testing.T) {
		aFunction := func() (Profile, Profile) {
			return Profile{33, "Meku"}, Profile{25, "Legese"}
		}

		var got []string
		want := []string{"Meku", "Legese"}

		walk(aFunction, func(input string) {
			got = append(got, input)
		})

		if !reflect.DeepEqual(got, want) {
			t.Errorf("got %v, want %v", got, want)
		}
	})

	// expected := "Meku"
	// var got []string
	// x := struct {
	// 	Name string
	// }{expected}
	// walk(x, func(input string) {
	// 	got = append(got, input)
	// })

	// if len(got) != 1 {
	// 	t.Errorf("wrong number of function calls, got %d want %d", len(got), 1)
	// }
	// if got[0] != expected {
	// 	t.Errorf("got %q, want %q", got[0], expected)
	// }

}
