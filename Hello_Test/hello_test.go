package main

import "testing"

func TestHello(t *testing.T) {
	got := Hello("Meku")
	want := "Hello Meku"

	if got != want {
		t.Errorf("Want : '%s' but Got: '%s'", want, got)
	}
}
