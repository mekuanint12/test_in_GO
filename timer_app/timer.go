package main

import (
	"flag"
	"fmt"
	"os"
	"time"
)

func main() {
	timeStringPtr := flag.String("time", "", "count down this time")
	flag.Parse()
	// localLocation, err := time.LoadLocation("Africa/AddisAbeba")
	// timeZone, _ := time.LoadLocation("")
	inputTime, err := time.Parse("15:04", *timeStringPtr)
	countdownTime := time.Now()
	countdownTime = time.Date(countdownTime.Year(), countdownTime.Month(), countdownTime.Day(), inputTime.Hour(), inputTime.Minute(), 0, 0, countdownTime.Location())
	if err != nil {
		os.Exit(1)
	}
	fmt.Printf("%s", countdownTime)
	tick := time.Tick(1000 * time.Millisecond)

	for delta := time.Until(countdownTime).Round(1000000000); delta > 0; delta = time.Until(countdownTime).Round(1000000000) {
		select {
		case <-tick:
			fmt.Println(delta, "left")
		}
	}
	fmt.Println("\n ⏲  COUNT DOWN OVER!!! ⏲")
}
