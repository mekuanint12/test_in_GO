package servertest

import (
	"log"
	"net/http"
)

// func main() {
// 	handler := http.HandlerFunc(PlayerServer)
// 	log.Fatal(http.ListenAndServe(":5000", handler))
// }

type InMemoryPlayerStore struct{}

func (i *InMemoryPlayerStore) GetPlayerScore(name string) int {
	return 123
}

func main() {
	server := &PlayerServer{&InMemoryPlayerStore{}}
	log.Fatal(http.ListenAndServe(":5000", server))
}
