package adder

import "testing"

func TestAdder(t *testing.T) {
	got := Adder(2, 2)
	want := 4

	if got != want {
		t.Errorf("Want : '%d' but Got : '%d'", want, got)
	}
}
