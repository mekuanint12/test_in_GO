package ArrSlice

import (
	"reflect"
	"testing"
)

func TestArrSliceSum(t *testing.T) {
	checkSums := func(t testing.TB, got, want []int) {
		t.Helper()
		if !reflect.DeepEqual(got, want) {
			t.Errorf("WANT : %v but GOT : %v ", want, got)
		}
	}

	t.Run("Slice of Any size", func(t *testing.T) {
		numbers := []int{10, 10, 10, 25}

		got := Sum(numbers)
		want := 55
		if got != want {
			t.Errorf("WANT : %v but GOT : %v ", want, got)
		}
	})
	t.Run("Adding Slices and Returning Slice", func(t *testing.T) {
		got := SumAll([]int{1, 2}, []int{0, 9}, []int{2, 2})
		want := []int{3, 9, 4}
		checkSums(t, got, want)
	})
	t.Run("make the sums of some Slices", func(t *testing.T) {
		got := TestSumAllTails([]int{1, 2, 4}, []int{0, 9, 1})
		want := []int{6, 10}
		checkSums(t, got, want)
	})
}
