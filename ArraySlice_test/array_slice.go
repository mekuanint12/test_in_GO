package ArrSlice

func Sum(nums []int) int {
	sum := 0
	// for i := 0; i < len(nums); i++ {
	for _, i := range nums {
		sum += i
	}
	return sum
}

func SumAll(slicesToSum ...[]int) []int {
	var sums []int
	for _, numbers := range slicesToSum {
		sums = append(sums, Sum(numbers))
	}
	return sums
}

func TestSumAllTails(slicesToSum ...[]int) []int {
	// lenOfNums := len(slicesToSum)
	// sums := make([]int, lenOfNums)
	var sums []int
	for _, tails := range slicesToSum {
		tail := tails[1:]
		sums = append(sums, Sum(tail))
	}
	return sums
}

// func SumAll(allSlices ...[]int) (sums []int) {
// 	add1 := 0
// 	add2 := 0
// 	sum := []int{add1, add2}
// 	for _, add := range a {
// 		add1 += add
// 	}
// 	for _, add := range b {
// 		add2 += add
// 	}
// 	return sum
// }
