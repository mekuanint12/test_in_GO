package depency_injection

import (
	"bytes"
	"testing"
)

func TestGreeting(t *testing.T) {
	buffer := bytes.Buffer{}
	Greet(&buffer, "Meku")
	got := buffer.String()
	want := "Hello, Meku"

	if got != want {
		t.Errorf("got %q want %q", got, want)
	}

}
