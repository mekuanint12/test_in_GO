package concurrency

import (
	"reflect"
	"testing"
	"time"
)

func mockWebsiteChecker(url string) bool {
	if url == "waat://furhurterwe.geds" {
		return false
	}
	return true
}

func slowWebsiteCheker(_ string) bool {
	time.Sleep(1 * time.Second)
	return true
}

func BenchmarkCheckWebsites(b *testing.B) {
	urls_list := make([]string, 100)
	for i := 0; i < len(urls_list); i++ {
		urls_list[i] = "a url"
	}
	b.ResetTimer()
	for i := 0; i < b.N; i++ {
		websiteChecker(slowWebsiteCheker, urls_list)
	}
}

func TestCheckWebsites(t *testing.T) {
	websites := []string{
		"http://google.com",
		"http://blog.gypsydave5.com",
		"waat://furhurterwe.geds",
	}
	got := websiteChecker(mockWebsiteChecker, websites)
	want := map[string]bool{
		"http://google.com":          true,
		"http://blog.gypsydave5.com": true,
		"waat://furhurterwe.geds":    false,
	}

	if !reflect.DeepEqual(want, got) {
		t.Fatalf("wanted %v, Got : %v", want, got)
	}

}
