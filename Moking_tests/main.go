package counter

import (
	"fmt"
	"io"
	"os"
	"time"
)

type Sleeper interface {
	Sleep()
}

type DefaultSleeper struct{}

func (d *DefaultSleeper) Sleep() {
	time.Sleep(1 * time.Second)
}

const finalWord = "Go!"
const countDownStarts = 3

// func Counter(out io.Writer, sleeper Sleeper) {
// 	for i := countDownStarts; i > 0; i-- {
// 		fmt.Fprintln(out, i)
// 		sleeper.Sleep()
// 	}
// 	fmt.Fprint(out, finalWord)
// }
func Countdown(out io.Writer, sleeper Sleeper) {
	for i := countDownStarts; i > 0; i-- {
		sleeper.Sleep()
	}

	for i := countDownStarts; i > 0; i-- {
		fmt.Fprintln(out, i)
	}

	fmt.Fprint(out, finalWord)
}

func main() {
	sleeper := &ConfigurableSleeper{1 * time.Second, time.Sleep}
	Countdown(os.Stdout, sleeper)
}

// import (
// 	"fmt"
// 	"io"
// 	"os"
// 	"time"
// )

// // import (
// // 	"fmt"
// // 	"time"
// // )

// const finalWord = "Go!"
// const countDownStarts = 3

// type spySleeper struct {
// 	Calls int
// }

// func (s SpySleeper) Sleep() {
// 	s.Calls++
// }

// func Counter(out io.Writer) {
// 	for i := countDownStarts; i > 0; i-- {
// 		time.Sleep(time.Millisecond * 1000)
// 		fmt.Fprint(out, i)
// 	}
// 	fmt.Fprint(out, finalWord)
// }

// func main() {
// 	Counter(os.Stdout)
// }

// // func TestCoun
