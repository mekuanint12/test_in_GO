package counter

import (
	"bytes"
	"reflect"
	"testing"
	"time"
)

// type spySleeper struct {
// 	Calls int
// }

// func (s *spySleeper) Sleep() {
// 	s.Calls++
// }
const write = "write"
const sleep = "sleep"

type SpyTime struct {
	durationSlept time.Duration
}

func (s *SpyTime) Sleep(duration time.Duration) {
	s.durationSlept = duration
}

type SpyCountdownOperations struct {
	Calls []string
}

func (s *SpyCountdownOperations) Sleep() {
	s.Calls = append(s.Calls, sleep)
}

func (s *SpyCountdownOperations) Write(p []byte) (n int, err error) {
	s.Calls = append(s.Calls, write)
	return
}

type ConfigurableSleeper struct {
	duration time.Duration
	sleep    func(time.Duration)
}

func (c *ConfigurableSleeper) Sleep() {
	c.sleep(c.duration)
}

func TestCountDownMocking(t *testing.T) {
	t.Run("Prints 3 to Go!", func(t *testing.T) {

		buffer := &bytes.Buffer{}
		spySleeper := &SpyCountdownOperations{}
		Countdown(buffer, spySleeper)

		got := buffer.String()
		want := `3
2
1
Go!`

		if got != want {
			t.Errorf("Got : %q Want : %q", got, want)
		}

		// if spySleeper.Calls != 3 {
		// 	t.Errorf("not enough calls to sleeper, want 3 but got %d calls", spySleeper.Calls)
		// }
	})

	t.Run("Sleep before every Print", func(t *testing.T) {
		spySleepPrinter := &SpyCountdownOperations{}
		Countdown(spySleepPrinter, spySleepPrinter)

		want := []string{
			write,
			sleep,
			write,
			sleep,
			write,
			sleep,
			write,
		}

		if !reflect.DeepEqual(want, spySleepPrinter.Calls) {
			t.Errorf("wanted calls %v got %v", want, spySleepPrinter.Calls)
		}
	})
	t.Run("Sleep Config", func(t *testing.T) {
		sleepTime := 5 * time.Second

		spyTime := &SpyTime{}
		sleeper := ConfigurableSleeper{sleepTime, spyTime.Sleep}
		sleeper.Sleep()

		if spyTime.durationSlept != sleepTime {
			t.Errorf("should have slept for %v but slept for %v", sleepTime, spyTime.durationSlept)
		}
	})
}

// import (
// 	"bytes"
// 	"testing"
// )

// type Sleeper interface {
// 	Sleep()
// }

// func TestCounter(t *testing.T) {
// 	buffer := &bytes.Buffer{}
// 	Counter(buffer)
// 	got := buffer.String()
// 	want := `
// 	3
// 	2
// 	1
// 	Go!`

// 	if got != want {
// 		t.Errorf("got : %s but Want : %s", got, want)
// 	}
// 	if spySleeper.Calls != 3 {
// 		t.Errorf("not enough Calls to sleeper, want 3 got %d", spySleeper.Calls)
// 	}
// }
