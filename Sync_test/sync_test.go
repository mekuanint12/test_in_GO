package synctest

import (
	"sync"
	"testing"
)

func assertCounter(t testing.TB, got Counter, want int) {
	t.Helper()
	if got.Value() != want {
		t.Errorf("got %d, want %d", got.Value(), 3)
	}
}

func NewCounter() *Counter {
	return &Counter{}
}

func TestCounter(t *testing.T) {
	t.Run("Incrementing the counter 3 times leaves it at 3", func(t *testing.T) {
		counter := Counter{}
		counter.Inc()
		counter.Inc()
		counter.Inc()

		assertCounter(t, *NewCounter(), 3)
		// if counter.Value() != 3 {
		//  	t.Errorf("got %d, want %d", counter.Value(), 3)
		// }
	})
	t.Run("It runs safely concurrently", func(t *testing.T) {
		wantedCout := 1000
		counter := Counter{}
		var wg sync.WaitGroup
		wg.Add(wantedCout)

		for i := 0; i < wantedCout; i++ {
			go func() {
				counter.Inc()
				wg.Done()
			}()
		}
		wg.Wait()

		assertCounter(t, *NewCounter(), wantedCout)
	})
}
