package pointersAndErrors

import (
	"errors"
	"fmt"
)

type Bitcoin int

type wallet struct {
	balance Bitcoin
}

type Stringer interface {
	String() string
}

func (b Bitcoin) String() string {
	return fmt.Sprintf("%d BTC", b)
}

func (w *wallet) Deposit(amount Bitcoin) {
	w.balance += amount
}

func (w *wallet) Balance() Bitcoin {
	return w.balance
}

func (w *wallet) Withdraw(amount Bitcoin) error {
	if amount > w.balance {
		return errors.New("oh no")
	}
	w.balance -= amount
	return nil
}
