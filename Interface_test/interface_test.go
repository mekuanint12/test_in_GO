package interfaces

import (
	"math"
	"testing"
)

type Rectangle struct {
	width  float64
	height float64
}

type Circle struct {
	radius float64
}
type Triangle struct {
	Base   float64
	Height float64
}

type Shape interface {
	Area() float64
}

func Perimeter(rectangle Rectangle) float64 {
	return 2 * (rectangle.width + rectangle.height)
}

func (r Rectangle) Area() float64 {
	return r.width * r.height
}

func (t Triangle) Area() float64 {
	return (t.Base * t.Height) * 0.5
}

func (c Circle) Area() float64 {
	return math.Pi * c.radius * c.radius
}
func TestPerimeter(t *testing.T) {
	rec := Rectangle{10.0, 10.0}
	got := Perimeter(rec)
	want := 40.0
	if got != want {
		t.Errorf("WANT : %.2f but GOT : %.2f ", want, got)
	}
}

// func TestArea(t *testing.T) {
// 	checkArea := func(t testing.TB, shape Shape, want float64) {
// 		t.Helper()
// 		got := shape.Area()
// 		if got != want {
// 			t.Errorf("WANT : %g but GOT : %g ", want, got)
// 		}
// 	}
// 	t.Run("rectangles", func(t *testing.T) {
// 		rec := Rectangle{12, 6}
// 		checkArea(t, rec, 72.0)
// 	})
// 	t.Run("circles", func(t *testing.T) {
// 		circle := Circle{10}
// 		checkArea(t, circle, 314.1592653589793)
// 	})
// }

func TestArea(t *testing.T) {
	areaTests := []struct {
		name  string
		shape Shape
		want  float64
	}{
		{name: "Rectangle", shape: Rectangle{12, 6}, want: 72.0},
		{name: "Circle", shape: Circle{10}, want: 314.1592653589793},
		{name: "Triangle", shape: Triangle{12, 6}, want: 36.0},
	}
	for _, tt := range areaTests {
		t.Run(tt.name, func(t *testing.T) {
			got := tt.shape.Area()
			if got != tt.want {
				t.Errorf("%#v GOT: %g but WANT %g", tt.shape, got, tt.want)
			}
		})
	}
}
