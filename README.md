# Learning Go with Tests

Exploring the Go language by writing tests

## Examples for testing

Create a new file called 'hello_test.go'

```go
package mekutest

import "testing"

func TestHello(t *testing.T) {
	got := Hello()
	want := "Hello, world"

	if got != want {
		t.Errorf("got %q want %q", got, want)
	}
}
```

## How to run the test

Type 'go test' in to the terminal

## Discipline

The cycles for testing are as follows

- Write a test
- Make the compiler pass
- Run the test, see that it fails and check the error message is meaningful
- Write enough code to make the test pass
- Refactor
