package loops

import "testing"

func TestIteration(t *testing.T) {
	got := Iteration("a")
	want := "aaaaaa"
	if got != want {
		t.Errorf("Want : '%s' bu GOT : '%s' ", want, got)
	}
}

func BenchmarkIteration(b *testing.B) {
	for i := 0; i < b.N; i++ {
		Iteration("v")
	}
}
