package mapsTest

import (
	"errors"
	"testing"
)

// func Search(dict map[string]string, word string) string {
// 	return dict[word]
// }

type Dictionary map[string]string

var dictionary = Dictionary{"test": "this is just test", "meku": "Mekuanint Legese"}

var ErrNotFound = errors.New("sorry, 😟 could not find the word you looking for")

func assertStrings(t testing.TB, want, got string) {

	t.Helper()
	if got != want {
		t.Errorf("Got : '%s' but Want : '%s'", got, want)
	}
}

func TestDictionary(t *testing.T) {

	t.Run("Word_Search", func(t *testing.T) {

		got, _ := dictionary.Search("test")
		want := "this is just test"
		assertStrings(t, want, got)
	})

	t.Run("Unknown_word", func(t *testing.T) {
		got, _ := dictionary.Search("Unknown")
		assertStrings(t, got, ErrNotFound.Error())
	})
	t.Run("Add_Definition", func(t *testing.T) {
		dictionary.Add("test1", "BOB Marly")
		want := "BOB Marly"
		got, err := dictionary.Search("test1")
		if err != nil {
			t.Fatal("should find add word: ", err)
		}
		assertStrings(t, want, got)
	})

}

func (d Dictionary) Search(w string) (string, error) {
	definition, ok := d[w]
	if !ok {
		return "", ErrNotFound
	}
	return definition, nil
}

func (d Dictionary) Add(word, definition string) {
	// d[word] = definition
}
